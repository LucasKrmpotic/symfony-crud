FROM php:7.3-apache

# Install all the system dependencies and enable PHP modules 

# libreoffice ghostscript
RUN apt-get update && apt-get install -y \
  python3 \
  graphviz \
  vim \
  git\
  wget

# mbstring extension

RUN docker-php-ext-configure mbstring \
  && docker-php-ext-install mbstring
# intl extension
RUN apt-get install -y zlib1g-dev libicu-dev g++ \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl
# mysql extension
RUN apt-get install -y mysql-client \
  && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
  && docker-php-ext-configure pdo_mysql \
  && docker-php-ext-install pdo_mysql
# zip extension
RUN apt-get install -y zip unzip zlib1g-dev libzip-dev \
  && docker-php-ext-configure zip \
  && docker-php-ext-install zip
# gd extension
RUN apt-get install -y libpng-dev \
  && docker-php-ext-configure gd \
  && docker-php-ext-install gd
# pcntl extension
RUN docker-php-ext-configure pcntl \
  && docker-php-ext-install pcntl
# xml/dom extension
RUN apt-get install -y libxml2-dev \
  && docker-php-ext-configure xml \
  && docker-php-ext-install xml
# soap extension
RUN docker-php-ext-configure soap \
  && docker-php-ext-install soap
# bcmath extension
RUN docker-php-ext-configure bcmath \
  && docker-php-ext-install bcmath

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer


RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN ln -s /root/.symfony/bin/symfony /usr/bin/symfony
RUN ln -s /root/.symfony/bin/symfony /bin/symfony

# Install xdebug
RUN yes | pecl install xdebug

    
# Set our application folder as an environment variable
ENV APP_HOME /var/www/html

# An IDE key has to be set, but anything works, at least for PhpStorm and VS Code...
#ENV XDEBUG_CONFIG="xdebug.idekey=PHPSTORM"

# Change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 985 www-data

# Enable apache module rewrite
RUN a2enmod rewrite

# Enable apache module headers (Commented for now, do not delete it!)
# RUN a2enmod headers

ADD "./php.ini" "/usr/local/etc/php/php.ini"